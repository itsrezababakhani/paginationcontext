import React, { useState } from "react";
import ThemeContext from "./ThemeContext";
import Header from "./components/Header";
import InfinitePhoto from "./components/InfinitePhoto";
import ThemeSwitcher from "./components/ThemeSwitcher";
import Paginator from "./components/Paginator";

function App() {
  const changeTheme = currentTheme => {
    if (theme === "light") {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  };
  const [theme, setTheme] = useState("light");
  return (
    <ThemeContext.Provider value={theme}>
      <div className={theme}>
        <Header />
        <Paginator />
        <ThemeSwitcher onSwitch={changeTheme} theme={theme} />
        <InfinitePhoto />
      </div>
    </ThemeContext.Provider>
  );
}

export default App;
