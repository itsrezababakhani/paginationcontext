import React, { useEffect, useState } from "react";
import HttpHandler from "../HttpHandler";
import Photo from "./Photo";

const http = new HttpHandler();
export default function InfinitePhoto() {
  const [data, setData] = useState([]);
  useEffect(() => {
    http.get("https://jsonplaceholder.ir/photos").then(result => {
      return setData(result.data);
    });
  }, data.length);
  const renderItem = data.map(item => {
    return <Photo title={item.title} image={item.thumbnailUrl} />;
  });
  return <div>{renderItem}</div>;
}
