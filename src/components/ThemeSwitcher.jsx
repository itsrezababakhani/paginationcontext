import React from "react";
export default function ThemeSwitcher(props) {
  return (
    <div className="switch__container">
      <input
        id="switch-shadow"
        className="switch switch--shadow"
        type="checkbox"
        onClick={() => {
          props.onSwitch(props.theme);
        }}
      />
      <label for="switch-shadow"></label>
    </div>
  );
}
