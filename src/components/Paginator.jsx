import React, { useState } from "react";
export default function Paginator({ totalPage, currentPage, onPageChange }) {
  const [page, setCurrentPage] = useState(1);
  const showPage = [...new Array(10).keys()].map(item => {
    item++;
    return (
      <li
        onClick={() => {
          setCurrentPage(item);
          console.log(page);
        }}
      >
        {item}
      </li>
    );
  });
  return showPage;
}
