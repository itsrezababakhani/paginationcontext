import React from "react";
import ThemeContext from "../ThemeContext";
export default function Photo({ title, image }) {
  return (
    <ThemeContext.Consumer>
      {value => {
        return (
          <div className={value}>
            <img src={image} alt="" />
            <p>{title}</p>
          </div>
        );
      }}
    </ThemeContext.Consumer>
  );
}
