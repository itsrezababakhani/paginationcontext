import axios from "axios";
import { Component } from "react";
class HttpHandler extends Component {
  get(url) {
    return axios.get(url);
  }
}
export default HttpHandler;
